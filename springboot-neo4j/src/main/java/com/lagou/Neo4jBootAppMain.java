package com.lagou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Neo4jBootAppMain {
    public static void main(String[] args) {
        ApplicationContext  applicationContext = SpringApplication.run(Neo4jBootAppMain.class,args);
//        Neo4jPersonService  personService = applicationContext.getBean("personService",Neo4jPersonService.class);
//        Person  person = new Person();
//        person.setName("testboot");
//        person.setMoney(12345.45);
//        person.setCharacter("A");
//        person.setAge(11);
//        Person p1 = personService.save(person);
//        System.out.println(p1);
//        System.out.println(personService.getAll());
//        List<Person>  personList = personService.personList(1000);
//        System.out.println(personList);
//        List<Person>  personList2 = personService.shortestPath("王启年","九品射手燕小乙");
//        System.out.println(personList2);
//        List<Person> personList3 = personService.personListDept("范闲");
//        for (Person pe:personList3){
//            System.out.println(pe);
//        }
    }
}
