package com.lagou.controller;

import com.lagou.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/neo4j")
public class Neo4jController {
    @Autowired
    private PersonRepository personRepository;
    @GetMapping
    public String neo4j(){
        return "neo4j";
    }

    @GetMapping("/getData")
    @ResponseBody
    public Object getData(){
        System.out.println(personRepository.personListDept("范闲"));
         return personRepository.personListDept("范闲");
    }
}
